export const CHAT_MODEL_ROLES = {
  user: 'user',
  system: 'system',
  assistant: 'assistant',
};

export const GENIE_CHAT_RESET_MESSAGE = '/reset';

export default {};

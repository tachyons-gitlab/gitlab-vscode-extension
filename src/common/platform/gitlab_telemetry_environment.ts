export interface GitLabTelemetryEnvironment {
  isTelemetryEnabled(): boolean;
}
